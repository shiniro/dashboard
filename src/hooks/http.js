import { useState, useEffect } from 'react';

export default httpClient => {
  const [error, setError] = useState(null);

    // componentWillMount () { Want to render before it's render but useEffect execute after so just remove it
    const reqInterceptor = httpClient.interceptors.request.use(req => {
      setError(null);
      return req;
    })
    const resInterceptor = httpClient.interceptors.response.use(res => res, error => {  // res => res : return res
      setError(error);
    })

    useEffect(() => {
      return () => { // clean up fonction
        httpClient.interceptors.request.eject(reqInterceptor);
        httpClient.interceptors.response.eject(resInterceptor);
      }
    }, [reqInterceptor, resInterceptor, httpClient]);

    const errorConfirmedHandler = () => {
      setError(null);
    }

    return [error, errorConfirmedHandler];
}