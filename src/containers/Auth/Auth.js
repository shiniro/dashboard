import React from 'react';

import image from '../../assets/img/social-monitoring.png';
import Input from '../../components/UI/Input/Input';
import Button from '../../components/UI/Button/Button';

import classes from './auth.module.scss';

const Auth = props => {
  return(
    <div className={classes.auth}>
      <div className={classes.content}>
        <div>
          <img src={image} alt="media monitoring" />
        </div>
        <div className={classes.formContainer}>
          <form>
            <h1>Dashboard</h1>
            <Input />
            <Input />
            <Button>Login</Button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default Auth;