import React from 'react';
import { Link } from 'react-router-dom';

import classes from './notFound.module.scss';

const NotFound = props => {
  return(
    <div className={classes.notFound}>
      <h1>404</h1>
      <h2>Page not found</h2>
      <Link to="/" className={classes.button}>Go to Homepage</Link>
    </div>
  );
}

export default NotFound;