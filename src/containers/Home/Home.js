import React from 'react';
import { connect } from 'react-redux';
import axios from 'axios';

import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';
import Spinner from '../../components/UI/Spinner/Spinner';

import classes from './home.module.scss';

const Home = props => {

  // const { token, userId } = props;

  let orders = <Spinner />


  return (
    <div className={classes.orders}>
      {orders}
    </div>
  );
}

const mapStateToProps = state => {
  return {
    token: state.auth.token,
    userId: state.auth.userId
  };
};

export default connect(mapStateToProps)(withErrorHandler(Home, axios));