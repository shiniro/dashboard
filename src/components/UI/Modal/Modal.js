import React from 'react';
import PropTypes from 'prop-types';

import Backdrop from '../Backdrop/Backdrop';

import classes from './modal.module.scss';

const Modal = props => {

  return (
    <React.Fragment>
      <Backdrop show={props.show}  clicked={props.closeModal} />
      <div
        className={classes.modal}
        style={{
          transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
          opacity: props.show ? '1' : '0'
        }}
      >
        {props.children}
      </div>
    </React.Fragment>
  )
};

Modal.propTypes = {
  show: PropTypes.bool,
  closeModal: PropTypes.func.isRequired
}


export default React.memo(Modal, (prevProps, nextProps) => nextProps.show === prevProps.show && nextProps.children === prevProps.children); // memo only update when props change