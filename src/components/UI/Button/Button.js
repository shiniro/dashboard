import React from 'react';
import PropTypes from 'prop-types';

import classes from './button.module.scss';

const button = (props) => (
  <button className={[classes.button, classes[props.btnTypes]].join(' ')} onClick={props.clicked} disabled={props.disabled}>
    {props.children}
  </button>
);

button.propTypes = {
  btnTypes: PropTypes.string,
  disabled: PropTypes.bool,
  clicked: PropTypes.func
}


export default button;