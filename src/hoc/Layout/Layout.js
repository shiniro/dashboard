import React from 'react';
import { connect } from 'react-redux';

// import Header from '../../components/Header';
// import SideDrawer from '../../components/Navigation/SideDrawer/SideDrawer';

import classes from './layout.module.scss';

const Layout = props => {

  // const [showSideDrawer, setShowSideDrawer] = useState(false);

  // const sideDrawerClosedHandler = () => {
  //   setShowSideDrawer(false);
  // }

  // const sideDrawerOpenedHandler = () => {
  //   setShowSideDrawer(!showSideDrawer);
  // }

  return (
    <React.Fragment>
      {/* <Header
        opened={sideDrawerOpenedHandler}
        isAuthenticated={props.isAuthenticated} />
      <SideDrawer
        show={showSideDrawer}
        closed={sideDrawerClosedHandler}
        isAuthenticated={props.isAuthenticated}
      /> */}
      <main className={classes.content}>
        { props.children }
      </main>
    </React.Fragment>
  );
}

const mapStateToProps = state => {
  return {
    isAuthenticated: state.auth.token !== null
  }
}

export default connect(mapStateToProps)(Layout);