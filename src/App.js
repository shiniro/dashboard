import React, { Suspense } from 'react';
import { Route, Switch, withRouter, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import * as actions from './store/actions'

import Layout from './hoc/Layout/Layout';

import './assets/scss/shared.scss';

const Auth = React.lazy(() => {
  return import('./containers/Auth/Auth');
})

const Home = React.lazy(() => {
  return import('./containers/Home/Home');
})

const NotFound = React.lazy(() => {
  return import('./containers/NotFound/NotFound');
})

const App = props => {

  console.log(props);

  // can't reach anything if not authenticated
  let routes = (
    <Switch>
      <Route path='/auth' render={props => <Auth {...props}/>} />
      <Redirect to="/auth"/>
    </Switch>
  )

  if(props.isAuthenticated) {
    routes = (
      <Switch>
        <Route path="/auth" render={props => <Auth {...props} />} />
        <Route path='/' render={props => <Home {...props}/>} />
        <Route path='/404' render={props => <NotFound {...props}/>} />
        <Redirect to="/404" />
      </Switch>
    );
  }

  let content = (
    <Layout>
      <Suspense fallback={<p>Loading ...</p>}>{routes}</Suspense>
    </Layout>
  )

  if(props.location.pathname === '/404' || props.location.path === '/auth') {
    content = <Suspense fallback={<p>Loading ...</p>}>{routes}</Suspense>
  }

  return (
    <div className="app">
      {content}
    </div>
  );
}

const mapStateToProps = state => {
  return {
    isAuthenticated: state.auth.token !== null
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onTryAutoSignup: () => dispatch(actions.authCheckState())
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));

